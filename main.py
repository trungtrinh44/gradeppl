if __name__ == '__main__':
    import os
    from upload import get_immediate_subdirectories, upload
    from randomize_students import randomize_student

    path = os.path.join(os.getcwd(), 'students')
    students = get_immediate_subdirectories(path)
    # number: The number of students will be chosen randomly, e.g, number = 6 will choose 6 students randomly
    graded_by = randomize_student(students=students, number=6)
    divide_at = 3  # the position that the list of students will be divided at, i.e, if 6 students are chosen, first 3 students will grade lexersuites, others will grade parsersuites
    upload(path, viewers=graded_by, divided_at=divide_at)
