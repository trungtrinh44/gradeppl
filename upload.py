from __future__ import print_function

import os

from Crypto.Cipher import ARC4
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from httplib2 import Http
from oauth2client import file, client, tools
import time
try:
    import argparse

    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None
import mimetypes


# get subdirs
def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]


def upload(cur_dir=None, viewers=None, divided_at=None):
    # add scala type
    mimetypes.add_type('application/vnd.google-apps.document',
                       '.scala', strict=False)
    log_file = open('log_file.txt', 'w')
    SCOPES = 'https://www.googleapis.com/auth/drive.file'
    store = file.Storage('storage.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('client_secret.json', SCOPES)
        creds = tools.run_flow(flow, store, flags) \
            if flags else tools.run_flow(flow, store)

    DRIVE = build('drive', 'v3', http=creds.authorize(Http()))
    if cur_dir is None:
        cur_dir = os.getcwd()
    log_file.write('Get all subdirectories from ' + cur_dir)
    subdirectories = get_immediate_subdirectories(cur_dir)

    # file to send in a dir
    # file_to_send = (
    #     ('LexerSuite.scala'),
    #     ('ParserSuite.scala'),
    # )

    # create parent folder Assignment 1
    folder_metadata = {'name': 'Assignment 1',
                       'mimeType': 'application/vnd.google-apps.folder'
                       }
    parent_folder = DRIVE.files().create(body=folder_metadata).execute()
    if parent_folder:
        log_file.write('\n' + 'Created folder "%s"' %
                       (folder_metadata['name']))

    # create subdirectoris in parent folder
    parser_message = 'Please grade this ParserSuite.'
    lexer_message = 'Please grade this LexerSuite.'

    for subdir in subdirectories:
        # create folder with name = subdir

        folder_metadata = {'name': subdir,
                           'mimeType': 'application/vnd.google-apps.folder',
                           'parents': [parent_folder.get('id')],
                           }
        folder = DRIVE.files().create(body=folder_metadata).execute()
        if folder:
            log_file.write('\n' + 'Created subfolder "%s"' %
                           (folder_metadata['name']))
        file_to_send = [name for name in os.listdir(
            os.path.join(cur_dir, subdir))]
        # create files in subdir
        for filename in file_to_send:
            enc = ARC4.new('01325789')
            # create metadata of file which will be uploaded
            metadata = {
                'name': enc.encrypt(subdir).hex() + '_' + filename + '.txt',
                'parents': [folder.get('id')],
                'viewersCanCopyContent': False,
                'mimeType': 'text/plain'
            }
            # upload file
            filepath = cur_dir + '/' + subdir + '/' + filename
            media = MediaFileUpload(filepath,
                                    resumable=True)
            res = DRIVE.files().create(body=metadata,
                                       media_body=media).execute()
            if res:
                log_file.write('\n' + 'Uploaded "%s"' % (filename))
                if viewers:
                    for idx in range(len(viewers[subdir])):
                        viewer = viewers[subdir][idx]
                        if divided_at and filename == 'LexerSuite.scala' and idx < divided_at:
                            while(1):
                                try:
                                    res_per = DRIVE.permissions().create(
                                        fileId=res['id'],
                                        body={
                                            'emailAddress': viewer + '@hcmut.edu.vn',
                                            'role': 'reader',
                                            'type': 'user'
                                        },
                                        sendNotificationEmail=True,
                                        emailMessage=lexer_message
                                    ).execute()
                                    break
                                except:
                                    time.sleep(1)
                                    pass
                            if res_per:
                                log_file.write('\n' +
                                               'Allowed user ' + viewer + ' to view file ' + filename + ' of student ' + subdir)
                        elif divided_at and filename == 'ParserSuite.scala' and idx >= divided_at:
                            while(1):
                                try:
                                    res_per = DRIVE.permissions().create(
                                        fileId=res['id'],
                                        body={
                                            'emailAddress': viewer + '@hcmut.edu.vn',
                                            'role': 'reader',
                                            'type': 'user'
                                        },
                                        sendNotificationEmail=True,
                                        emailMessage=parser_message
                                    ).execute()
                                    break
                                except:
                                    time.sleep(1)
                                    pass
                            if res_per:
                                log_file.write('\n' +
                                               'Allowed user ' + viewer + ' to view file ' + filename + ' of student ' + subdir)

    log_file.close()


if __name__ == "__main__":
    upload(os.path.join(os.getcwd(), 'students'))
