# coding: utf-8


import json
from random import shuffle

# set to_file to True as default to export excel file


def randomize_student(students, number, to_file=True):
    # students = pd.read_excel(file_name)[col_name].map(lambda x: str(x))
    shuffle(students)
    d = dict()
    size = len(students)
    for idx in range(len(students)):
        key = students[idx]
        d[key] = []
        for i in range(1, number + 1):
            d[key].append(students[(idx + i) % size])

    if to_file:
        import pandas as pd

        def to_excel(name, data):
            df = pd.DataFrame.from_dict(data, 'index')
            df.to_excel(name)

        to_excel('MSSV_nguoichambai.xlsx', d)
    return d

#
# if __name__ == '__main__':
#     import sys
#
#     randomize_student(sys.argv[1], sys.argv[2], int(sys.argv[3]), True)
