# Assignment Phase 1 Grading Suite

### Main modules
This repository contains 3 main python modules:
- randomize_students.py will randomly assign 1 student to grade 3 other students' LexerSuites and another 3 students' ParserSuites.
- upload.py will handle upload all students' suite and grant respective student permission to view the suite.
- main.py will call the above 2 modules and automate everything.


### Dependencies:
- Python 3.6
- Install packages:
```
pip install --upgrade google-api-python-client httplib2 oauth2client pycrypto
```


### Manual:
- To run the code, first put all the folders' contain the LexerSuite.scala and ParserSuite.scala of each student inside the folder named 'students', each folder must be named after the respective student ID. For example, if we have 3 student whose student ID are 1414216, 1414316, 1412961 then the folder tree will be like this:

```
project
|    randomize_students.py
|    upload.py
|    main.py
|    client_secret.json
└────students
     └───1414216
     │   │   LexerSuite.scala
     │   │   ParserSuite.scala
     └───1414316
     │   │   LexerSuite.scala
     │   │   ParserSuite.scala
     └───1412961
         │   LexerSuite.scala
         │   ParserSuite.scala
```
- Then run 'main.py' . On the first run, the google api will automatically open the browser and ask you to sign in. After sign in, a new file call 'storage.json' will be created containing the authorization information for subsequent calls. If you want to change the authorized account, just the delete the storage.json file.
- All the actions will be logged in the 'log_file.txt' for validation.
- Note: Remember to delete the example folders in the students folder.